import json

from collections.abc import Mapping
from io import StringIO
from typing import Final

from ciutilsregistry.abc import CiUtilsRegistryAuthCfg
from ciutilsregistry.abc import CiUtilsRegistryCfg
from pygencollection.utils import CollectionUtils
from pygenpath.abc import Dirpath
from pygentype.abc import FinalSequence
from pygentype.abc import NamedPair
from pygentype.utils import TypeUtils


class CiUtilsRegistryUtils(object):
    FILEPATH: Final[str] = "./registry.json"

    @classmethod
    def cfg_to_dict(cls, cfg: CiUtilsRegistryCfg) -> object:
        return {
            "auths": CollectionUtils.map_to_dict(
                items=CollectionUtils.pairs(cfg.auths),
                fn=lambda pair: (pair.key, TypeUtils.asdict(pair.value))
            )
        }

    @classmethod
    def cfg_to_text(cls, cfg: CiUtilsRegistryCfg) -> str:
        buffer = StringIO()

        buffer.write(json.dumps(cls.cfg_to_dict(cfg)))
        buffer.write("\n")

        return buffer.getvalue()

    @classmethod
    def load_cfg(cls, dirpath: Dirpath) -> CiUtilsRegistryCfg:
        with (open(dirpath.resolve_filepath(cls.FILEPATH).path()) as f):
            return cls._create_cfg(**json.loads(f.read()))

    @classmethod
    def save_cfg(cls, dirpath: Dirpath, cfg: CiUtilsRegistryCfg) -> None:
        with (open(dirpath.resolve_filepath(cls.FILEPATH).path(), "w") as f):
            f.write(cls.cfg_to_text(cfg))

    @classmethod
    def _create_cfg(
        cls,
        auths: Mapping[str, Mapping[str, object]]
    ) -> CiUtilsRegistryCfg:
        return CiUtilsRegistryCfg(
            auths=CollectionUtils.map_to_dict(
                items=CollectionUtils.pairs(auths),
                fn=lambda pair: NamedPair(
                    key=pair.key,
                    value=CiUtilsRegistryAuthCfg(**pair.value)
                )
            )
        )


__all__: FinalSequence[str] = [
    "CiUtilsRegistryUtils"
]

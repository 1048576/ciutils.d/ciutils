from typing import Dict
from typing import List

from cu.pipeline.abstract import Pipeline
from cu.pipeline.abstract import PipelineFactory
from pygencli.parser.abstract import CliArg
from pygencli.parser.abstract import CliParser
from pygencli.parser.arg.impl import CliArgImpl
from pygencli.parser.cmd.impl import CliCmdParserImpl
from pygenpath.abstract import PathResolver
from pygenpath.impl import PathResolverImpl


class GitlabPipelineFactoryImpl(PipelineFactory):
    _ARG_WORKDIR: CliArg = CliArgImpl("CI_PROJECT_DIR")
    _ARG_JOB_NAME: CliArg = CliArgImpl("CI_JOB_NAME")

    _parser: CliParser

    def __init__(self) -> None:
        parser = CliCmdParserImpl.create()

        parser.add_key_arg(
            arg=self._ARG_WORKDIR,
            flag="ci-project-dir"
        )

        parser.add_key_arg(
            arg=self._ARG_JOB_NAME,
            flag="ci-job-name"
        )

        self._parser = parser

    def create(
        self,
        argv: List[str],
        envs: Dict[str, str]
    ) -> Pipeline:
        args = self._parser.parse(argv, envs)
        self._fetch_workdir(args)
        self._fetch_job_name(args)

        return Pipeline()

    def _fetch_workdir(self, args: Dict[str, str]) -> PathResolver:
        arg = self._ARG_WORKDIR
        with (arg):
            workdir = arg.fetch(args)

            return PathResolverImpl.create(workdir)

    def _fetch_job_name(self, args: Dict[str, str]) -> str:
        arg = self._ARG_JOB_NAME
        with (arg):
            return arg.fetch(args)

import json

from argparse import Namespace
from typing import Dict

import cu.core.http.requests as requests

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.artifact import Artifact
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorageFactory


class TriggerCallCommand():
    storage_factory: CiUtilsStorageFactory

    flag_token: ArgFlagArgument
    flag_ref: ArgFlagArgument
    flag_url: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.flag_token = parser.add_flag_argument(
            flag_name="token",
            env_name="CU_TRIGGER_TOKEN"
        )

        self.flag_ref = parser.add_flag_argument(
            flag_name="ref",
            env_name="CU_TRIGGER_REF"
        )

        self.flag_url = parser.add_flag_argument(
            flag_name="url",
            env_name="CU_TRIGGER_URL"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        namespace = storage.get_namespace()
        version = storage.get_version()
        branch = storage.get_branch()
        token = self.fetch_token(ns)
        ref = self.fetch_ref(ns)
        url = self.fetch_url(ns)

        artifact = Artifact(
            namespace=namespace,
            version=version
        )

        variables: Dict[str, str] = {
           "variables[BUILD_ARTIFACT_ID]": artifact.id(),
           "variables[BUILD_BRANCH]": branch
        }

        logger.info("Ref: [%s]", [ref])
        logger.info("Url: [%s]", [url])
        for k, v in sorted(variables.items()):
            logger.info(k.capitalize() + ": " + v)

        response = requests.send(
            method="POST",
            url=url,
            form={
                **{
                    "ref": ref,
                    "token": token
                },
                **variables
            }
        )

        if (response.status_code in (200, 201)):
            parsed: Dict[str, object] = json.loads(response.content)

            logger.info(
                "Pipeline run successfully [%s]" % parsed.pop("web_url")
            )
        else:
            raise ExpectedException("Status code [%s]", [response.status_code])

    def fetch_token(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_token, ns)

    def fetch_ref(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_ref, ns)

    def fetch_url(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_url, ns)

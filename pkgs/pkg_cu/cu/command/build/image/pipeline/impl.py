import cu.pipeline.utils

from cu.command.build.image.pipeline.abstract import BuildImagePipeline
from pygenarg.address.abstract import AddressArg
from pygenarg.path.abstract import PathResolverArg
from pygenarg.text.abstract import TextArg
from pygenarg.text.impl import TextArgImpl


class BuildImagePipelineImpl(BuildImagePipeline):
    @staticmethod
    def create(
        pipeline_id_arg: TextArg,
        workdir_arg: PathResolverArg,
        service_address_arg: AddressArg,
        context_arg: TextArg,
        dockerfile_arg: TextArg,
        target_arg: TextArg,
        registry_image_arg: TextArg,
        registry_username_arg: TextArg,
        registry_password_arg: TextArg
    ) -> BuildImagePipeline:
        with (pipeline_id_arg as arg):
            pipeline_id = cu.pipeline.utils.pipeline_id(
                value=arg.value()
            )

        with (service_address_arg as arg):
            service_address = cu.pipeline.utils.service_address(
                value=arg.value()
            )

        with (context_arg as arg):
            context = TextArgImpl(
                description="Context",
                value=arg.value()
            )

        with (dockerfile_arg as arg):
            dockerfile = TextArgImpl(
                description="Dockerfile",
                value=arg.value()
            )

        with (target_arg as arg):
            target = TextArgImpl.create_text_required(
                description="Target",
                value=arg.value()
            )

        with (registry_image_arg as arg):
            registry_image = TextArgImpl.create_text_required(
                description="Registry image",
                value=arg.value()
            )

        with (registry_username_arg as arg):
            registry_username = TextArgImpl.create_text_required(
                description="Registry username",
                value=arg.value()
            )

        with (registry_password_arg as arg):
            registry_password = TextArgImpl.create_text_required(
                description="Registry password",
                value=arg.value()
            )

        return BuildImagePipelineImpl(
            pipeline_id=pipeline_id,
            service_address=service_address,
            context=context,
            dockerfile=dockerfile,
            target=target,
            registry_image=registry_image,
            registry_username=registry_username,
            registry_password=registry_password
        )

    _pipeline_id: TextArg
    _service_address: AddressArg
    _context: TextArg
    _dockerfile: TextArg
    _target: TextArg
    _registry_image: TextArg
    _registry_username: TextArg
    _registry_password: TextArg

    def __init__(
        self,
        pipeline_id: TextArg,
        service_address: AddressArg,
        context: TextArg,
        dockerfile: TextArg,
        target: TextArg,
        registry_image: TextArg,
        registry_username: TextArg,
        registry_password: TextArg
    ) -> None:
        self._pipeline_id = pipeline_id
        self._service_address = service_address
        self._context = context
        self._dockerfile = dockerfile
        self._target = target
        self._registry_image = registry_image
        self._registry_username = registry_username
        self._registry_password = registry_password

    def pipeline_id(self) -> TextArg:
        return self._pipeline_id

    def service_address(self) -> AddressArg:
        return self._service_address

    def context(self) -> TextArg:
        return self._context

    def dockerfile(self) -> TextArg:
        return self._dockerfile

    def target(self) -> TextArg:
        return self._target

    def registry_image(self) -> TextArg:
        return self._registry_image

    def registry_username(self) -> TextArg:
        return self._registry_username

    def registry_password(self) -> TextArg:
        return self._registry_password

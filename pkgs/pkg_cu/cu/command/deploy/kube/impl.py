from typing import List

from cu.command.abstract import Command
from cu.command.deploy.kube.pipiline.abstract import DeployKubePipeline
from pygenlog.logger.abstract import Logger
from pygenshell.remote.abstract import RemoteShell
from pygenshell.std.stream.adapter.impl import ShellStdStreamAdapterImpl


class DeployKubeCommandImpl(Command):
    _shell: RemoteShell
    _logger: Logger
    _pipeline: DeployKubePipeline

    def __init__(
        self,
        shell: RemoteShell,
        logger: Logger,
        pipeline: DeployKubePipeline
    ) -> None:
        self._shell = shell
        self._logger = logger
        self._pipeline = pipeline

    def run(self) -> None:
        workdir = self._pipeline.workdir()
        http_proxy = self._pipeline.http_proxy()
        https_proxy = self._pipeline.https_proxy()
        no_proxy = self._pipeline.no_proxy()
        service_address = self._pipeline.service_address()
        kubeconfig = self._pipeline.kubeconfig()
        environment = self._pipeline.environment()
        app_version = self._pipeline.app_version()

        deploy_kube_resolver = workdir.value().cd("./deploy-kube.d/")

        http_proxy.describe(self._logger)
        https_proxy.describe(self._logger)
        no_proxy.describe(self._logger)
        workdir.describe(self._logger)
        service_address.describe(self._logger)
        kubeconfig.describe(self._logger)
        environment.describe(self._logger)
        app_version.describe(self._logger)

        playbook_path = deploy_kube_resolver.resolve(
            path="./install.yaml"
        )

        inventory_resolver = deploy_kube_resolver.cd("./environments/")
        inventory_path = inventory_resolver.resolve(
            path="./%s.yaml" % (environment.value())
        )

        call_args: List[str] = [
            "K8S_AUTH_KUBECONFIG=%s" % (kubeconfig.value()),
            "ansible-playbook",
            playbook_path,
            "--inventory",
            inventory_path,
            "--extra-vars",
            "CU_DEPLOY_KUBE_APP_VERSION=%s" % (app_version.value())
        ]

        self._shell.call(
            address=service_address.value(),
            args=call_args,
            stdout=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            ),
            stderr=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            )
        )

from cu.pipeline.abstract import Pipeline
from pygenarg.address.abstract import AddressArg
from pygenarg.path.abstract import PathResolverArg
from pygenarg.text.abstract import TextArg


class DeployKubePipeline(Pipeline):
    def workdir(self) -> PathResolverArg:
        ...

    def http_proxy(self) -> TextArg:
        ...

    def https_proxy(self) -> TextArg:
        ...

    def no_proxy(self) -> TextArg:
        ...

    def service_address(self) -> AddressArg:
        ...

    def kubeconfig(self) -> TextArg:
        ...

    def environment(self) -> TextArg:
        ...

    def app_version(self) -> TextArg:
        ...


import cu.pipeline.utils

from cu.command.deploy.kube.pipiline.abstract import DeployKubePipeline
from pygenarg.address.abstract import AddressArg
from pygenarg.path.abstract import PathResolverArg
from pygenarg.text.abstract import TextArg
from pygenarg.text.impl import TextArgImpl


class DeployKubePipelineImpl(DeployKubePipeline):
    @staticmethod
    def create(
        workdir_arg: PathResolverArg,
        http_proxy_arg: TextArg,
        https_proxy_arg: TextArg,
        no_proxy_arg: TextArg,
        service_address_arg: AddressArg,
        kubeconfig_arg: TextArg,
        environment_arg: TextArg,
        app_version_arg: TextArg
    ) -> DeployKubePipeline:
        with (workdir_arg as arg):
            workdir = cu.pipeline.utils.workdir(arg.value())

        with (http_proxy_arg as arg):
            http_proxy = cu.pipeline.utils.http_proxy(arg.value())

        with (https_proxy_arg as arg):
            https_proxy = cu.pipeline.utils.https_proxy(arg.value())

        with (no_proxy_arg as arg):
            no_proxy = cu.pipeline.utils.no_proxy(arg.value())

        with (service_address_arg as arg):
            service_address = cu.pipeline.utils.service_address(
                value=arg.value()
            )

        with (kubeconfig_arg as arg):
            kubeconfig = TextArgImpl.create_text_required(
                description="Kubeconfig",
                value=arg.value()
            )

        with (environment_arg as arg):
            environment = TextArgImpl.create_text_required(
                description="Environment",
                value=arg.value()
            )

        with (app_version_arg as arg):
            app_version = cu.pipeline.utils.app_version(arg.value())

        return DeployKubePipelineImpl(
            workdir=workdir,
            http_proxy=http_proxy,
            https_proxy=https_proxy,
            no_proxy=no_proxy,
            service_address=service_address,
            kubeconfig=kubeconfig,
            environment=environment,
            app_version=app_version
        )

    _workdir: PathResolverArg
    _http_proxy: TextArg
    _https_proxy: TextArg
    _no_proxy: TextArg
    _service_address: AddressArg
    _kubeconfig: TextArg
    _environment: TextArg
    _app_version: TextArg

    def __init__(
        self,
        workdir: PathResolverArg,
        http_proxy: TextArg,
        https_proxy: TextArg,
        no_proxy: TextArg,
        service_address: AddressArg,
        kubeconfig: TextArg,
        environment: TextArg,
        app_version: TextArg
    ) -> None:
        self._workdir = workdir
        self._http_proxy = http_proxy
        self._https_proxy = https_proxy
        self._no_proxy = no_proxy
        self._service_address = service_address
        self._kubeconfig = kubeconfig
        self._environment = environment
        self._app_version = app_version

    def workdir(self) -> PathResolverArg:
        return self._workdir

    def http_proxy(self) -> TextArg:
        return self._http_proxy

    def https_proxy(self) -> TextArg:
        return self._https_proxy

    def no_proxy(self) -> TextArg:
        return self._no_proxy

    def service_address(self) -> AddressArg:
        return self._service_address

    def kubeconfig(self) -> TextArg:
        return self._kubeconfig

    def environment(self) -> TextArg:
        return self._environment

    def app_version(self) -> TextArg:
        return self._app_version

from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.command.registry.login_cmd import RegistryLoginCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger


class RegistryCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_login: RegistryLoginCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="REGISTRY-COMMAND"
        )

        self.cmd_login = RegistryLoginCommand(
            subparsers.add_parser(
                name="login",
                help="Login"
            )
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        command = self.arg_cmd.value(ns)
        if (command == "login"):
            self.cmd_login.run(logger, ns)
        else:
            raise ExpectedException("Unknown registry command [%s]", [command])

import re

from typing import Dict
from typing import List
from typing import Pattern
from typing import Tuple

import pygencli.parser.utils

from cu.storage.abstract import StorageFactory
from pygencli.parser.abstract import CliArg
from pygencli.parser.arg.impl import CliArgImpl
from pygenerr.expected.exception import ExpectedException
from pygenlog.log.abstract import Logger
from pygenpath.resolver.abstract import PathResolver
from pygenpath.resolver.impl import PathResolverImpl
from pygenshell.remote.abstract import RemoteShell
from pygenshell.std.stream.adapter.impl import ShellStdStreamAdapterImpl


class ImageBuildCommand:
    RE_TARGET: Pattern[str] = re.compile(r"[a-z]+(?:\-[a-z]+)*")

    ARG_KANIKO_ADDRESS: CliArg = CliArgImpl("CU_KANIKO_ADDRESS")

    ARG_IMAGE_BUILD_CONTEXT: CliArg = CliArgImpl("CU_IMAGE_BUILD_CONTEXT")
    ARG_IMAGE_BUILD_FILE: CliArg = CliArgImpl("CU_IMAGE_BUILD_FILE")
    ARG_IMAGE_BUILD_TARGET: CliArg = CliArgImpl("CU_IMAGE_BUILD_TARGET")

    ARG_HTTP_PROXY: CliArg = CliArgImpl("HTTP_PROXY")
    ARG_HTTPS_PROXY: CliArg = CliArgImpl("HTTPS_PROXY")
    ARG_NO_PROXY: CliArg = CliArgImpl("NO_PROXY")

    _storage_factory: StorageFactory
    _shell: RemoteShell
    _logger: Logger

    def __init__(
        self,
        storage_factory: StorageFactory,
        shell: RemoteShell,
        logger: Logger
    ) -> None:
        self._storage_factory = storage_factory
        self._shell = shell
        self._logger = logger

    def run(self, args: Dict[str, str]) -> None:
        storage = self._storage_factory.attach(args)
        workspace = storage.workspace()
        pipeline_id = storage.pipeline_id()
        registry_image = storage.registry_image()

        workspace_resolver = PathResolverImpl.create(workspace)

        digest_file = workspace_resolver.resolve("./.digest")
        kaniko_address = self._fetch_kaniko_address(args)
        target = self._fetch_target(args)
        context_resolver = self._fetch_context(workspace_resolver, args)
        context = context_resolver.pwd()
        dockerfile = self._fetch_file(context_resolver, args)
        cache_repo = "%s/cache" % (registry_image)
        destination = "%s/tmp/%s-%s" % (registry_image, target, pipeline_id)
        build_args = storage.build_args()

        self._logger.info("Workspace: [%s]", [workspace])
        self._logger.info("Target: [%s]", [target])
        self._logger.info("Kaniko address: [%s:%d]", list(kaniko_address))
        self._logger.info("Context: [%s]", [context])
        self._logger.info("Build file: [%s]", [dockerfile])
        self._logger.info("Cache repo: [%s]", [cache_repo])
        self._logger.info("Image tag: [%s]", [destination])
        for k, v in build_args:
            self._logger.info("Build arg [%s]: [%s]" % (k, v))

        call_args: List[str] = [
            "/kaniko/executor",
            "--skip-unused-stages",
            "--digest-file", digest_file,
            "--target", target,
            "--context", context,
            "--dockerfile", dockerfile,
            "--cache",
            "--cache-repo", cache_repo,
            "--destination", destination
        ]
        for k, v in build_args:
            call_args.append("--build-arg %s='%s'" % (k, v))

        self._shell.call(
            address=kaniko_address,
            args=call_args,
            stdout=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            ),
            stderr=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            )
        )

    def _fetch_kaniko_address(
        self,
        args: Dict[str, str]
    ) -> Tuple[str, int]:
        arg = ImageBuildCommand.ARG_KANIKO_ADDRESS
        with (arg):
            return pygencli.parser.utils.fetch_address(arg, args)

    def _fetch_target(self, args: Dict[str, str]) -> str:
        arg = ImageBuildCommand.ARG_IMAGE_BUILD_TARGET
        with (arg):
            target = arg.fetch(args)
            if (not ImageBuildCommand.RE_TARGET.match(target)):
                raise ExpectedException(
                   msg_template="Not match [%s]",
                   msg_vars=[ImageBuildCommand.RE_TARGET.pattern]
                )

            return target

    def _fetch_context(
        self,
        workspace_resolver: PathResolver,
        args: Dict[str, str]
    ) -> PathResolver:
        arg = ImageBuildCommand.ARG_IMAGE_BUILD_CONTEXT
        with (arg):
            value = arg.fetch(args)
            resolver = workspace_resolver.cd(value)
            path = resolver.pwd()
            workspace_path = workspace_resolver.pwd()
            if (not path.startswith(workspace_path)):
                raise ExpectedException(
                    msg_template="Context [%s] out of workspace [%s]",
                    msg_vars=[
                        path,
                        workspace_path
                    ]
                )

        return resolver

    def _fetch_file(
        self,
        context_resolver: PathResolver,
        args: Dict[str, str]
    ) -> str:
        arg = ImageBuildCommand.ARG_IMAGE_BUILD_FILE
        with (arg):
            value = arg.fetch(args)
            path = context_resolver.resolve(value)
            context_path = context_resolver.pwd()
            if (not path.startswith(context_path)):
                raise ExpectedException(
                    msg_template="Image file [%s] out of context [%s]",
                    msg_vars=[
                        path,
                        context_path
                    ]
                )

        return path

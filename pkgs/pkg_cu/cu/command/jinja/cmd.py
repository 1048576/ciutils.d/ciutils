from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.command.jinja.render_cmd import JinjaRenderCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger


class JinjaCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_render: JinjaRenderCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="JINJA-COMMAND"
        )

        self.cmd_render = JinjaRenderCommand(
            subparsers.add_parser(
                name="render",
                help="Render"
            )
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        jinja_command = self.arg_cmd.value(ns)
        if (jinja_command == "render"):
            self.cmd_render.run(logger, ns)
        else:
            raise ExpectedException(
                "Unknown jinja command [%s]" % jinja_command
            )

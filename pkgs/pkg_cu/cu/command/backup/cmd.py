from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.command import Command
from cu.core.git import Git
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorageFactory


class BackupCommand(CliCommand):
    storage_factory: CiUtilsStorageFactory

    flag_ssh_private_key: ArgFlagArgument
    flag_ssh_known_hosts: ArgFlagArgument
    flag_git_backup_server: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.flag_ssh_private_key = parser.add_flag_argument(
            flag_name="ssh-private-key",
            env_name="CU_SSH_PRIVATE_KEY"
        )
        self.flag_ssh_known_hosts = parser.add_flag_argument(
            flag_name="ssh-known-hosts",
            env_name="CU_SSH_KNOWN_HOSTS"
        )
        self.flag_git_backup_server = parser.add_flag_argument(
            flag_name="git-backup-server",
            env_name="CU_GIT_BACKUP_SERVER"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        workspace = storage.get_workspace()
        git = Git.create(
            git_server_host=self.fetch_git_backup_server(ns),
            project_path=storage.get_project_path(),
            flag_ssh_private_key=self.flag_ssh_private_key,
            flag_ssh_known_hosts=self.flag_ssh_known_hosts,
            ns=ns
        )

        logger.info("Workspace: [%s]", [workspace])
        logger.info("Git backup repo: [%s]", [git.repo])
        logger.info("Private key: [%s]", [git.ssh_private_key])
        logger.info("Known hosts: [%s]", [git.ssh_known_hosts])

        push_cmd = Command.local(
            "git",
            "-C",
            workspace,
            "push",
            git.repo,
            "HEAD:refs/heads/main"
        )

        push_cmd.call_check(
            logger=logger,
            env=git.env,
            suppress_stderr=False
        )

    def fetch_git_backup_server(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_git_backup_server, ns)

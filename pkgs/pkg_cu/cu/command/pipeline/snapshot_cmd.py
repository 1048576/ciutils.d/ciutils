import json

from argparse import Namespace
from typing import Tuple

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.command import Command
from cu.core.errors import ExpectedException
from cu.core.git import Git
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorage
from cu.storage.impl import CiUtilsStorageFactory


class PipelineSnapshotCommand:
    storage_factory: CiUtilsStorageFactory

    flag_ssh_private_key: ArgFlagArgument
    flag_ssh_known_hosts: ArgFlagArgument
    flag_user_name: ArgFlagArgument
    flag_user_email: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.flag_ssh_private_key = parser.add_flag_argument(
            flag_name="ssh-private-key",
            env_name="CU_SSH_PRIVATE_KEY"
        )
        self.flag_ssh_known_hosts = parser.add_flag_argument(
            flag_name="ssh-known-hosts",
            env_name="CU_SSH_KNOWN_HOSTS"
        )
        self.flag_user_name = parser.add_flag_argument(
            flag_name="user-name",
            env_name="GITLAB_USER_NAME"
        )
        self.flag_user_email = parser.add_flag_argument(
            flag_name="user-email",
            env_name="GITLAB_USER_EMAIL"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        workspace = storage.get_workspace()
        namespace = storage.get_namespace()
        version = storage.get_version()
        git = Git.create(
            git_server_host=storage.get_git_server_host(),
            project_path=storage.get_project_path(),
            flag_ssh_private_key=self.flag_ssh_private_key,
            flag_ssh_known_hosts=self.flag_ssh_known_hosts,
            ns=ns
        )
        user_name, user_email = self.fetch_author(ns)

        if (namespace != "rc"):
            raise ExpectedException("Unsupported namespace [%s]", [namespace])

        tag = "v%s-%s" % (version, namespace)

        logger.info("Namespace: [%s]", [namespace])
        logger.info("Workspace: [%s]", [workspace])
        logger.info("Tag: [%s]", [tag])
        logger.info("Author: [%s <%s>]", [user_name, user_email])
        logger.info("Git repo: [%s]", [git.repo])
        logger.info("Private key: [%s]", [git.ssh_private_key])
        logger.info("Known hosts: [%s]", [git.ssh_known_hosts])

        self.create_snapshot(
            logger=logger,
            storage=storage,
            workspace=workspace
        )

        self.commit(
            logger=logger,
            workspace=workspace,
            user_name=user_name,
            user_email=user_email,
            tag=tag
        )

        self.push(
            logger=logger,
            workspace=workspace,
            git=git,
            tag=tag
        )

    def fetch_author(self, ns: Namespace) -> Tuple[str, str]:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return (
            validator.verify(self.flag_user_name, ns),
            validator.verify(self.flag_user_email, ns)
        )

    def create_snapshot(
        self,
        logger: Logger,
        storage: CiUtilsStorage,
        workspace: str
    ) -> None:
        snapshot_file = workspace + ".snapshot.json"
        with open(snapshot_file, "w") as f:
            snapshot = json.dumps(
                obj={
                    "manifests": storage.get_manifests()
                },
                indent=2
            )
            f.write(snapshot)

        cmd_add = Command.local(
            "git",
            "-C",
            workspace,
            "add",
            "-f",
            snapshot_file
        )
        cmd_add.call_check(logger)

    def commit(
        self,
        logger: Logger,
        workspace: str,
        user_name: str,
        user_email: str,
        tag: str
    ) -> None:
        commit_message = "Version " + tag

        cmd_commit = Command.local(
            "git",
            "-C",
            workspace,
            "commit",
            "-m",
            commit_message
        )

        cmd_commit.call_check(
            logger=logger,
            env={
                "GIT_AUTHOR_EMAIL": user_email,
                "GIT_AUTHOR_NAME": user_name,
                "GIT_COMMITTER_EMAIL": user_email,
                "GIT_COMMITTER_NAME": user_name
            }
        )

    def push(
        self,
        logger: Logger,
        workspace: str,
        git: Git,
        tag: str
    ) -> None:
        ref = "HEAD:refs/tags/%s" % tag
        cmd_push = Command.local("git", "-C", workspace, "push", git.repo, ref)
        cmd_push.call_check(
            logger=logger,
            env=git.env,
            suppress_stderr=False
        )

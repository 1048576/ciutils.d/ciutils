import re

from argparse import Namespace
from typing import Pattern

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.parser import ArgParser
from cu.core.arg.parser import ArgPositionalArgument
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorageFactory


class FetchArgsFromBranchCommand():
    storage_factory: CiUtilsStorageFactory

    arg_pattern: ArgPositionalArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.arg_pattern = parser.add_positional_argument(
            metavar="PATTERN",
            help="Regular expression"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        workspace = storage.get_workspace()
        branch = storage.get_branch()
        compiled_pattern = self.fetch_pattern(ns)

        logger.info("Workspace: [%s]", [workspace])
        logger.info("Commit branch: [%s]", [branch])
        logger.info("Pattern: [%s]", [compiled_pattern.pattern])

        matched = compiled_pattern.match(branch)
        if (matched is None):
            raise ExpectedException(
                msg_template="Commit branch [%s] not match [%s]",
                msg_vars=[
                    branch,
                    compiled_pattern.pattern
                ]
            )

        for k, v in sorted(matched.groupdict().items()):
            storage.add_arg(k, v)
            logger.info("Arg [%s]: [%s]", [k, v])

    def fetch_pattern(self, ns: Namespace) -> Pattern[str]:
        validator = ArgArgumentValidator(
            Validators.re_validator(r".+")
        )

        pattern = "^" + validator.verify(self.arg_pattern, ns) + "$"

        try:
            return re.compile(pattern)
        except Exception:
            raise ExpectedException("Invalid pattern [%s]" % pattern)

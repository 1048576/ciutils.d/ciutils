import json

from argparse import Namespace
from typing import Dict

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.parser import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.storage.abstract import AbstractStorage
from cu.core.storage.etcd import Etcd
from cu.core.validator import Validators

WORKSPACE = "/workspace"
GIT_SERVER_HOST = "/git-server-host"
PROJECT_PATH = "/project-path"
REGISTRY_IMAGE = "/registry-image"
NAMESPACE = "/namespace"
BRANCH = "/branch"
VERSION = "/version"
ARGS = "/args/"
MANIFESTS = "/manifests/"


class CiUtilsStorage:
    storage: AbstractStorage
    pipeline_id: str

    def __init__(self, storage: AbstractStorage, pipeline_id: str) -> None:
        self.storage = storage.child("/" + pipeline_id)
        self.pipeline_id = pipeline_id

    def set_workspace(self, value: str) -> None:
        self.storage.put(WORKSPACE, value)

    def get_workspace(self) -> str:
        return self.storage.get(WORKSPACE)

    def set_git_server_host(self, value: str) -> None:
        self.storage.put(GIT_SERVER_HOST, value)
        self.add_arg("BUILD_ARG_GIT_SERVER_HOST", value)

    def get_git_server_host(self) -> str:
        return self.storage.get(GIT_SERVER_HOST)

    def set_project_path(self, value: str) -> None:
        self.storage.put(PROJECT_PATH, value)

    def get_project_path(self) -> str:
        return self.storage.get(PROJECT_PATH)

    def set_registry_image(self, value: str) -> None:
        self.storage.put(REGISTRY_IMAGE, value)

    def get_registry_image(self) -> str:
        return self.storage.get(REGISTRY_IMAGE)

    def set_namespace(self, value: str) -> None:
        self.storage.put(NAMESPACE, value)
        self.add_arg("BUILD_ARG_NAMESPACE", value)

    def get_namespace(self) -> str:
        return self.storage.get(NAMESPACE)

    def set_branch(self, value: str) -> None:
        self.storage.put(BRANCH, value)

    def get_branch(self) -> str:
        return self.storage.get(BRANCH)

    def set_version(self, value: str) -> None:
        self.storage.put(VERSION, value)
        self.add_arg("BUILD_ARG_VERSION", value)

    def get_version(self) -> str:
        return self.storage.get(VERSION)

    def add_arg(self, key: str, value: str) -> None:
        self.storage.put(ARGS + key, json.dumps({"k": key, "v": value}))

    def get_args(self) -> Dict[str, str]:
        args: Dict[str, str] = {}

        for arg in self.storage.get_prefix(ARGS):
            arg_parsed = json.loads(arg)
            args[arg_parsed.get("k")] = arg_parsed.get("v")

        return args

    def add_manifest(self, name: str, manifest: object) -> None:
        self.storage.put(
            path=MANIFESTS + name,
            value=json.dumps({"name": name, "manifest": manifest})
        )

    def get_manifests(self) -> Dict[str, Dict[str, object]]:
        manifests: Dict[str, Dict[str, object]] = {}

        for manifest in self.storage.get_prefix(MANIFESTS):
            manifest_parsed = json.loads(manifest)
            manifests[manifest_parsed.get("name")] = manifest_parsed.get("manifest")

        return manifests


class CiUtilsStorageFactory:
    flag_storage_address: ArgFlagArgument
    flag_pipeline_id: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.flag_storage_address = parser.add_flag_argument(
            flag_name="storage-address",
            env_name="CU_STORAGE_ADDRESS"
        )
        self.flag_pipeline_id = parser.add_flag_argument(
            flag_name="pipeline-id",
            env_name="CI_PIPELINE_ID"
        )

    def create(self, ns: Namespace) -> CiUtilsStorage:
        storage_address_validator = ArgArgumentValidator(
            Validators.re_validator(r"(?:[a-z][a-z0-9]+)(\.[a-z][a-z0-9]+)*\:[1-9][0-9]+")
        )
        storage_address = storage_address_validator.verify(self.flag_storage_address, ns)

        pipeline_id_validator = ArgArgumentValidator(
            Validators.re_validator(r"\d+")
        )
        pipeline_id = pipeline_id_validator.verify(self.flag_pipeline_id, ns)

        host, port = tuple(storage_address.split(":"))

        etcd = Etcd.create(host, int(port))

        return CiUtilsStorage(etcd, pipeline_id)

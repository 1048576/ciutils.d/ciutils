from __future__ import annotations

import os
import socket
import subprocess

from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple

from cu.core.errors import ExpectedException
from cu.core.exception.unexpected import UnexpectedException
from cu.core.logger import Logger


class CompletedProcess:
    returncode: int
    stdout: bytes
    stderr: bytes

    def __init__(self, returncode: int, stdout: bytes, stderr: bytes) -> None:
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr

    def stdout_as_text(self) -> str:
        return self.stdout.decode()

    def stderr_as_text(self) -> str:
        return self.stderr.decode()


class Executor:
    def call(
        self,
        logger: Logger,
        cmd: List[str],
        env: Dict[str, str],
        input: Optional[bytes],
        suppress_stdout: bool,
        suppress_stderr: bool
    ) -> CompletedProcess: ...


class LocalExecutor(Executor):
    def call(
        self,
        logger: Logger,
        cmd: List[str],
        env: Dict[str, str],
        input: Optional[bytes],
        suppress_stdout: bool,
        suppress_stderr: bool
    ) -> CompletedProcess:
        try:
            stdout = subprocess.PIPE if suppress_stdout else None
            stderr = subprocess.PIPE if suppress_stderr else None

            completed_process = subprocess.run(
                cmd,
                input=input,
                env={**os.environ, **env},
                stderr=stderr,
                stdout=stdout
            )

            return CompletedProcess(
                returncode=completed_process.returncode,
                stdout=completed_process.stdout,
                stderr=completed_process.stderr if suppress_stderr else b""
            )
        except FileNotFoundError as e:
            raise ExpectedException.create_and_wrap(
                msg_template=str(e),
                title_template="Invalid call %s",
                title_vars=[cmd]
            )


Address = Tuple[str, int]


class Buffer:
    source: socket.socket
    buffer: bytes
    index: int

    def __init__(self, source: socket.socket) -> None:
        self.source = source
        self.buffer = b""
        self.index = 0

    def is_empty(self) -> bool:
        if (self.index == len(self.buffer)):
            self.buffer = self.source.recv(128)
            self.index = 0

        return (len(self.buffer) == 0)

    def read(self) -> int:
        if self.is_empty():
            raise UnexpectedException("Empty buffer")
        else:
            try:
                return self.buffer[self.index]
            finally:
                self.index += 1


class RemoteExecutor(Executor):
    address: Address

    def __init__(self, address: Address) -> None:
        self.address = address

    def call(
        self,
        logger: Logger,
        cmd: List[str],
        env: Dict[str, str],
        input: Optional[bytes],
        suppress_stdout: bool,
        suppress_stderr: bool
    ) -> CompletedProcess:
        marker = "returncode: "
        cmd_as_bytes = " ".join(cmd).encode()
        cmd_as_bytes += " 2>&1; echo {}$?\n".format(marker).encode()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                try:
                    s.connect(self.address)
                    s.sendall(cmd_as_bytes)

                    buffer = Buffer(s)
                    line: str = ""
                    while (not buffer.is_empty()):
                        char_as_bytes = b""
                        while True:
                            try:
                                char_as_bytes += bytes([buffer.read()])
                                char = char_as_bytes.decode()
                                if (char == "\n"):
                                    if line.startswith(marker):
                                        return CompletedProcess(
                                            returncode=int(line[len(marker):]),
                                            stdout=b"",
                                            stderr=b""
                                        )
                                    logger.info(
                                        text_template="%s",
                                        text_vars=[line],
                                        colored=False
                                    )
                                    line = ""
                                else:
                                    line += char
                                break
                            except UnicodeDecodeError:
                                pass
                except socket.gaierror as e:
                    raise ExpectedException(e.strerror)
                except ConnectionRefusedError as e:
                    raise ExpectedException(e.strerror)
                except ConnectionResetError as e:
                    raise ExpectedException(e.strerror)
            except ExpectedException as e:
                raise e.wrap(
                    title_template="Cannot connect to [%s:%d]",
                    title_vars=list(self.address)
                )

        return CompletedProcess(
            returncode=1,
            stdout=b"",
            stderr=b"Process interrupted"
        )


class Command:
    @staticmethod
    def local(*cmd: str) -> Command:
        return Command(LocalExecutor(), *cmd)

    @staticmethod
    def remote(address: Tuple[str, int], *cmd: str) -> Command:
        return Command(RemoteExecutor(address), *cmd)

    executor: Executor
    cmd: List[str]

    def __init__(self, executor: Executor, *cmd: str) -> None:
        self.executor = executor
        self.cmd = list(cmd)

    def add(self, value: str) -> Command:
        self.cmd.append(value)

        return self

    def call_output(self, logger: Logger, env: Dict[str, str] = {}) -> str:
        return self.call_check(logger, env).stdout_as_text()

    def call_check(
        self,
        logger: Logger,
        env: Dict[str, str] = {},
        input: Optional[bytes] = None,
        suppress_stdout: bool = True,
        suppress_stderr: bool = True
    ) -> CompletedProcess:
        completed_process = self.call(
            logger=logger,
            env=env,
            input=input,
            suppress_stdout=suppress_stdout,
            suppress_stderr=suppress_stderr
        )

        if (completed_process.returncode != 0):
            if suppress_stderr:
                raise ExpectedException.create_and_wrap(
                    msg_template=completed_process.stderr.decode(),
                    title_template="Command returned non-zero exit code [%s]",
                    title_vars=[completed_process.returncode]
                )
            else:
                raise ExpectedException(
                    msg_template="Command returned non-zero exit code [%s]",
                    msg_vars=[completed_process.returncode]
                )

        return completed_process

    def call(
        self,
        logger: Logger,
        env: Dict[str, str] = {},
        input: Optional[bytes] = None,
        suppress_stdout: bool = True,
        suppress_stderr: bool = True
    ) -> CompletedProcess:
        return self.executor.call(
            logger=logger,
            cmd=self.cmd,
            env=env,
            input=input,
            suppress_stdout=suppress_stdout,
            suppress_stderr=suppress_stderr
        )

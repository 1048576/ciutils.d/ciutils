from __future__ import annotations

from typing import Dict

from cu.core.errors import ExpectedException


class WWWAuthenticate:
    @staticmethod
    def parse(text: str) -> WWWAuthenticate:
        try:
            return WWWAuthenticate.do_parse(text)
        except Exception:
            raise ExpectedException(
                "Cannot parse www-authenticate: [%s]" % text
            )

    @staticmethod
    def do_parse(text: str) -> WWWAuthenticate:
        it = iter(text)

        scheme = ""
        while True:
            c = next(it)
            if (c == " "):
                break
            scheme += c

        options: Dict[str, str] = {}
        k, c = ("", next(it))
        try:
            while True:
                while (c != "="):
                    k += c
                    c = next(it)

                if (next(it) != "\""):
                    raise Exception()

                v, c = ("", next(it))
                while (c != "\""):
                    v += c
                    c = next(it)

                options[k] = v
                k = ""

                c = next(it)
                if (c == ","):
                    c = next(it)
        except StopIteration:
            if (k == ""):
                return WWWAuthenticate(scheme, options.pop("realm"), options)
            raise Exception()

    scheme: str
    realm: str
    options: Dict[str, str]

    def __init__(
        self,
        scheme: str,
        realm: str,
        options: Dict[str, str]
    ) -> None:
        self.scheme = scheme
        self.realm = realm
        self.options = options


class QueryBuilder:
    pairs: Dict[str, str]

    def __init__(self, pairs: Dict[str, str]) -> None:
        self.pairs = pairs

    def build(self) -> str:
        query = ""
        it = iter(sorted(self.pairs.items()))
        try:
            k, v = next(it)
            query += k + "=" + v
            while True:
                k, v = next(it)
                query += "&" + k + "=" + v
        except StopIteration:
            return query

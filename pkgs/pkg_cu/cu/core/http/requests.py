from typing import Dict
from typing import Optional

from requests.models import Response


class HTTPBasicAuth:
    username: str
    password: str

    def __init__(self, username: str, password: str) -> None:
        self.username = username
        self.password = password


def send(
    method: str,
    url: str,
    auth: Optional[HTTPBasicAuth] = None,
    headers: Optional[Dict[str, str]] = None,
    json: Optional[object] = None,
    form: Optional[Dict[str, str]] = None
) -> Response:
    import requests
    import requests.models

    auth_origin = None
    if (auth is not None):
        auth_origin = requests.models.HTTPBasicAuth(
            username=auth.username,
            password=auth.password
        )

    args: Dict[str, object] = {
        "method": method,
        "url": url,
        "auth": auth_origin,
        "headers": headers,
        "json": json,
        "data": form
    }

    return requests.api.request(**args)  # type: ignore

from __future__ import annotations

import os

from argparse import Namespace
from typing import Dict
from typing import Tuple

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.validator import Validators


class Git:
    @staticmethod
    def create(
        git_server_host: str,
        project_path: str,
        flag_ssh_private_key: ArgFlagArgument,
        flag_ssh_known_hosts: ArgFlagArgument,
        ns: Namespace
    ) -> Git:
        repo = "git@%s:%s" % (git_server_host, project_path)

        ssh_private_key, ssh_known_hosts = Git.fetch_ssh(
            flag_ssh_private_key=flag_ssh_private_key,
            flag_ssh_known_hosts=flag_ssh_known_hosts,
            ns=ns
        )
        os.chmod(ssh_private_key, mode=0o600)

        return Git(repo, ssh_private_key, ssh_known_hosts)

    @staticmethod
    def fetch_ssh(
        flag_ssh_private_key: ArgFlagArgument,
        flag_ssh_known_hosts: ArgFlagArgument,
        ns: Namespace
    ) -> Tuple[str, str]:
        validator = ArgArgumentValidator(
            Validators.file_path_validator()
        )

        return (
            validator.verify(flag_ssh_private_key, ns),
            validator.verify(flag_ssh_known_hosts, ns)
        )

    repo: str
    ssh_private_key: str
    ssh_known_hosts: str
    env: Dict[str, str]

    def __init__(
        self,
        repo: str,
        ssh_private_key: str,
        ssh_known_hosts: str
    ) -> None:
        self.repo = repo
        self.ssh_private_key = ssh_private_key
        self.ssh_known_hosts = ssh_known_hosts
        git_ssh_cmd = "ssh -i {} -o UserKnownHostsFile={}".format(
            ssh_private_key,
            ssh_known_hosts
        )
        self.env = {
            "GIT_SSH_COMMAND": git_ssh_cmd
        }

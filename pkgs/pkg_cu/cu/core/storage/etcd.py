from __future__ import annotations

from typing import Callable
from typing import Generator
from typing import TypeVar

from cu.core.errors import ExpectedException
from cu.core.storage.abstract import AbstractStorage
from etcd3 import Etcd3Client  # type: ignore
from etcd3.exceptions import ConnectionFailedError  # type: ignore


class Etcd(AbstractStorage):
    T = TypeVar("T")

    @staticmethod
    def create(host: str, port: int) -> Etcd:
        return Etcd(Etcd3Client(host, port), "")

    client: Etcd3Client
    prefix: str

    def __init__(self, client: Etcd3Client, prefix: str) -> None:
        self.client = client
        self.prefix = prefix

    def child(self, prefix: str) -> AbstractStorage:
        return Etcd(self.client, self.prefix + prefix)

    def get(self, path: str) -> str:
        value: bytes = self.execute(lambda: self.client.get(self.prefix + path)[0])  # type: ignore

        return value.decode()

    def get_prefix(self, path: str) -> Generator[str, None, None]:
        generator = self.execute(lambda: self.client.get_prefix(self.prefix + path))  # type: ignore

        while True:
            try:
                value_as_bytes: bytes = self.execute(lambda: next(generator)[0])  # type: ignore

                yield value_as_bytes.decode()
            except StopIteration:
                break

    def put(self, path: str, value: str) -> None:
        self.execute(lambda: self.client.put(self.prefix + path, value))  # type: ignore

    def execute(self, callback: Callable[[], T]) -> T:
        try:
            return callback()
        except ConnectionFailedError as e:
            raise ExpectedException.create_and_wrap(
                msg_template=str(e),
                title_template="Cannot execute operation on storage"
            )

import os.path

from typing import Tuple

from cu.core.errors import ExpectedException


def norm(path: str) -> str:
    if path.startswith("/"):
        normpath = os.path.normpath(path)
    elif (path == "./"):
        normpath = "."
    else:
        normpath = "./" + os.path.normpath(path)

    if path.endswith("/"):
        normpath += "/"

    if (normpath != path):
        raise ExpectedException("Invalid path format [%s]", [path])

    return normpath


def abs(path: str, ws: str) -> str:
    norm(path)
    norm(ws)

    if (not ws.startswith("/") or not ws.endswith("/")):
        raise ExpectedException("Invalid path format [%s]", [ws])

    if path.startswith("./"):
        return ws + path[len("./"):]
    else:
        return path


def abs_src_dst(src: str, dst: str, ws: str) -> Tuple[str, str]:
    abs_src = abs(src, ws)
    abs_dst = abs(dst, ws)

    if (abs_dst.endswith("/") and (not abs_src.endswith("/"))):
        abs_dst = os.path.join(abs_dst, os.path.basename(abs_src))

    return (abs_src, abs_dst)

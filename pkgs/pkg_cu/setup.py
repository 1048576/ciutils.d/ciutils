#!/usr/bin/env python

import os

from setuptools import find_packages
from setuptools import setup


def read_file(file_name: str) -> str:
    with open(file_name, "r") as f:
        return f.read()


setup(
    name="ciutils",
    version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
    author="Vladyslav Kazakov",
    author_email="kazakov1048576@gmail.com",
    url="https://gitlab.com/1048576/ciutils.d/ciutils",
    package_data={
        "cu": ["py.typed"]
    },
    packages=find_packages(
        include=["cu", "cu.*"]
    ),
    scripts=[
        "bin/ciutils-gitlab"
    ],
    install_requires=read_file("requirements.txt").splitlines(),
    license="MIT"
)

from pygenargnet.abc import NetworkAddressArg
from pygenargpath.abc import DirpathArg
from pygenargpath.abc import FilepathArg
from pygenargtext.abc import TextArg
from pygentype.abc import FinalSequence
from pygentype.err import SystemException


class CiUtilsClientBuildImageCommand(object):
    def run(
        self,
        configdir: DirpathArg,
        kaniko_address: NetworkAddressArg,
        context: DirpathArg,
        dockerfile: FilepathArg,
        target: TextArg,
        registry_repo: TextArg
    ) -> None:
        raise SystemException()


__all__: FinalSequence[str] = [
    "CiUtilsClientBuildImageCommand"
]

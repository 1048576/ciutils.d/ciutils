from typing import Final

from ciutilsclient.abc import CiUtilsClientShellOutput
from ciutilsclient.command.registrylogin.abc import CiUtilsClientRegistryLoginCommand
from ciutilsregistry.abc import CiUtilsRegistryAuthCfg
from ciutilsregistry.utils import CiUtilsRegistryUtils
from pygenargpath.abc import DirpathArg
from pygenargtext.abc import TextArg
from pygentype.abc import FinalSequence


class CiUtilsClientRegistryLoginCommandImpl(CiUtilsClientRegistryLoginCommand):
    _shell_output: Final[CiUtilsClientShellOutput]

    def __init__(
        self,
        shell_output: CiUtilsClientShellOutput
    ) -> None:
        self._shell_output = shell_output

    def run(
        self,
        configdir: DirpathArg,
        registry: TextArg,
        username: TextArg,
        password: TextArg
    ) -> None:
        self._shell_output.info(configdir.describe())
        self._shell_output.info(registry.describe())
        self._shell_output.info(username.describe())

        cfg = CiUtilsRegistryUtils.load_cfg(configdir.value())

        cfg.auths[registry.value()] = CiUtilsRegistryAuthCfg(
            username=username.value(),
            password=password.value()
        )

        CiUtilsRegistryUtils.save_cfg(configdir.value(), cfg)


__all__: FinalSequence[str] = [
    "CiUtilsClientRegistryLoginCommandImpl"
]

from pygencli.abc import CliArg
from pygenshell.abc import ShellOutput
from pygentype.abc import FinalSequence
from pygentype.err import SystemException


class CiUtilsClientShellOutput(ShellOutput):
    def info(self, msg: str) -> None:
        raise SystemException()


class CiUtilsClientEnvironment(object):
    def registry_repo_arg(self) -> CliArg:
        raise SystemException()

    def registry_login_registry_arg(self) -> CliArg:
        raise SystemException()

    def registry_login_username_arg(self) -> CliArg:
        raise SystemException()

    def registry_login_password_arg(self) -> CliArg:
        raise SystemException()


__all__: FinalSequence[str] = [
    "CiUtilsClientEnvironment",
    "CiUtilsClientShellOutput"
]

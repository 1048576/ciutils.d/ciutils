from ciutilsclient.abc import CiUtilsClientEnvironment
from pygencli.abc import CliArg


class CiUtilsGitlabEnvironmentImpl(CiUtilsClientEnvironment):
    def registry_repo_arg(self) -> CliArg:
        return CliArg("CI_REGISTRY_IMAGE")

    def registry_login_registry_arg(self) -> CliArg:
        return CliArg("CI_REGISTRY")

    def registry_login_username_arg(self) -> CliArg:
        return CliArg("CI_REGISTRY_USER")

    def registry_login_password_arg(self) -> CliArg:
        return CliArg("CI_REGISTRY_PASSWORD")

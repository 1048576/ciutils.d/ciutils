import selectors

from collections.abc import Mapping
from collections.abc import Sequence
from subprocess import Popen
from typing import Final

from ciutilsserver.abc import CiUtilsServer
from ciutilsserver.abc import CiUtilsServerRunner
from pygencli.abc import CliArg
from pygencli.abc import CliOption
from pygencli.abc import CliParser
from pygencli.abc import CliRootSimpleCmd
from pygencli.utils import CliUtils
from pygennet.utils import NetworkUtils
from pygenshell.abc import ShellOutput
from pygenshell.abc import ShellOutputEntry
from pygenshell.abc import ShellOutputSource
from pygenshellserver.abc import ShellServerProcessor
from pygenshellserver.impl import ShellServerImpl
from pygensocket.abc import ServerSocket
from pygensocket.impl import ServerSocketImpl
from pygensocket.impl import SocketPairImpl
from pygentype.abc import ImmutableSequence
from pygentype.err import SystemException


class CiUtilsServerProcessorImpl(ShellServerProcessor):
    def process(
        self,
        args: ImmutableSequence[str],
        output: ShellOutput
    ) -> int:
        if (args[0] == "upload"):
            try:
                _, path, content = args

                with (open(path, "w") as f):
                    f.write(content)

                return 0
            except Exception as e:
                output.write(
                    entry=ShellOutputEntry(
                        source=ShellOutputSource.STDERR,
                        data=str(e)
                    )
                )

                return 1
        else:
            return self._external_call(args, output)

    def _external_call(
        self,
        args: ImmutableSequence[str],
        output: ShellOutput
    ) -> int:
        stdout = SocketPairImpl()
        stderr = SocketPairImpl()

        process = Popen(
            args=args,
            stdout=stdout.write().fileno(),
            stderr=stderr.write().fileno(),
            shell=False
        )

        selector = selectors.DefaultSelector()
        selector.register(stdout.read(), selectors.EVENT_READ)
        selector.register(stderr.read(), selectors.EVENT_READ)

        with (stdout, stderr):
            while (True):
                for key, _ in selector.select(0.1):
                    if (key.fileobj is stdout.read()):
                        source = ShellOutputSource.STDOUT
                        pair = stdout
                    elif (key.fileobj is stderr.read()):
                        source = ShellOutputSource.STDERR
                        pair = stderr
                    else:
                        raise SystemException()

                    entry = ShellOutputEntry(
                        source=source,
                        data=pair.read().receive(4096).decode()
                    )

                    output.write(entry)

                status = process.poll()

                if (status is not None):
                    break

        selector.unregister(stdout.read())
        selector.unregister(stderr.read())

        return status


class CiUtilsServerImpl(CiUtilsServer):
    _server_socket: Final[ServerSocket]

    def __init__(self, server_socket: ServerSocket) -> None:
        self._server_socket = server_socket

    def serve_forever(self) -> None:
        while (True):
            with (self._server_socket.accept() as socket):
                shell_server = ShellServerImpl(
                    socket=socket,
                    processor=CiUtilsServerProcessorImpl()
                )

                shell_server.serve_forever()


class CiUtilsServerRunnerImpl(CiUtilsServerRunner):
    ADDRESS_ARG: Final[CliArg] = CliArg(
        metavar="CIUTILS_SERVER_ADDRESS"
    )

    _parser: Final[CliParser]

    def __init__(self) -> None:
        self._parser = CliUtils.parser(
            command=CliRootSimpleCmd(
                options=ImmutableSequence(
                    CliOption(
                        name="address",
                        arg=self.ADDRESS_ARG
                    )
                )
            )
        )

    def create(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> CiUtilsServer:
        return self._create(
            args=self._parser.parse(args, envs)
        )

    def _create(self, args: Mapping[str, str]) -> CiUtilsServer:
        with (CliUtils.value(self.ADDRESS_ARG, args) as value):
            address = NetworkUtils.create(value)

        server_socket = ServerSocketImpl()
        server_socket.bind(address)
        server_socket.listen(1)

        return CiUtilsServerImpl(
            server_socket=server_socket
        )

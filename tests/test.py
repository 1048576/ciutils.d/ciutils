import unittest

import pygenworkspace.utils

from pygenworkspace.impl import WorkspaceImpl


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    suite = pygenworkspace.utils.unittest_suite()

    workspace_dirpath = pygenworkspace.utils.workspace_dirpath(__file__)
    repo_url = "https://gitlab.com/1048576/ciutils.d/ciutils"

    pygenworkspace.utils.load_tests(
        test_suite=suite,
        workspace_dirpath=workspace_dirpath,
        repo_url=repo_url,
        workspaces=[
            WorkspaceImpl(
                workspace_dirpath=workspace_dirpath,
                repo_url=repo_url,
                dirnames=[
                    "pkgs"
                ],
                flake8_local_plugins_extensions=[],
                flake8_local_plugins_paths=[]
            )
        ]
    )

    return suite


if (__name__ == "__main__"):
    unittest.main()

#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Pre install
_sh "apk add --no-cache --virtual .build-deps curl bash git openssl"
# Install helm
_sh "curl -fsSL -o /tmp/get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3"
_sh "chmod u+x /tmp/get_helm.sh"
_sh "/tmp/get_helm.sh --version ${BUILD_ARG_HELM_VERSION}"
_sh "rm /tmp/get_helm.sh"
echo -e "\033[0;32mInstalled helm:\n$(helm version)\033[0m"

# Install helm-diff
_sh "helm plugin install https://github.com/databus23/helm-diff"

# Post install
_sh "apk del .build-deps"
_sh "rm $0"

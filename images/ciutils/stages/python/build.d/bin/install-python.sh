#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _cd {
    local path=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ cd ${path}\e[39m"
    cd ${path}
}

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

CIUTILS_LIB_DIRPATH="${BUILD_ARG_CIUTILS_DIRPATH}lib/"
CIUTILS_LINKER_FILEPATH="${CIUTILS_LIB_DIRPATH}ld.so"

PYTHON_BIN="/bin/python${BUILD_ARG_PYTHON_VERSION%.*}"
PYTHON_SOURCE_ARCHIVE_FILEPATH="${BUILD_ARG_WORKSPACE_DIRPATH}cpython-${BUILD_ARG_PYTHON_VERSION}.tar.gz"
PYTHON_SOURCE_DIRPATH="${BUILD_ARG_WORKSPACE_DIRPATH}cpython-${BUILD_ARG_PYTHON_VERSION}/"
PYTHON_SOURCE_URL="https://github.com/python/cpython/archive/refs/tags/v${BUILD_ARG_PYTHON_VERSION}.tar.gz"

_sh "apk add --no-cache --virtual .build-dependencies build-base libffi-dev openssl-dev zlib-dev"

# download python source
_sh "wget ${PYTHON_SOURCE_URL} -O ${PYTHON_SOURCE_ARCHIVE_FILEPATH}"
_sh "tar xf ${PYTHON_SOURCE_ARCHIVE_FILEPATH} -C ${BUILD_ARG_WORKSPACE_DIRPATH}"
_sh "rm ${PYTHON_SOURCE_ARCHIVE_FILEPATH}"

# install python in /
_cd "${PYTHON_SOURCE_DIRPATH}"
_sh "./configure --prefix=/"
_sh "make install --jobs=$(nproc)"
_cd "${BUILD_ARG_WORKSPACE_DIRPATH}"

# install pip
_sh "${PYTHON_BIN} -m ensurepip --upgrade"

# install python in /ciutils/
_sh "mkdir -p ${CIUTILS_LIB_DIRPATH}"
_sh "cp /lib/ld-musl-x86_64.so.1 ${CIUTILS_LINKER_FILEPATH}"
_cd "${PYTHON_SOURCE_DIRPATH}"
_sh "LDFLAGS='-Xlinker --dynamic-linker=${CIUTILS_LINKER_FILEPATH}' ./configure --prefix=${BUILD_ARG_CIUTILS_DIRPATH} --without-ensurepip"
_sh "make install DEST=${BUILD_ARG_CIUTILS_DIRPATH} --jobs=$(nproc)"
_cd "${BUILD_ARG_WORKSPACE_DIRPATH}"

# install other packages
_sh "apk add --no-cache libffi git"

# .post
_sh "apk del .build-dependencies"
_sh "rm $0"
_sh "rm -rf ${BUILD_ARG_WORKSPACE_DIRPATH}"

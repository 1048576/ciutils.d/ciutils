#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "pip3 install ${BUILD_ARG_CIUTILS_CLIENT_SOURCE_DIRPATH}"
_sh "rm -rf ${BUILD_ARG_CIUTILS_CLIENT_SOURCE_DIRPATH}"
_sh "pip3 install ${BUILD_ARG_CIUTILS_REGISTRY_SOURCE_DIRPATH}"
_sh "rm -rf ${BUILD_ARG_CIUTILS_REGISTRY_SOURCE_DIRPATH}"

# .pre
_sh "rm $0"
